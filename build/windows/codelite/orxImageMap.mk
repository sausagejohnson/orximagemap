##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x64
ProjectName            :=orxImageMap
ConfigurationName      :=Debug_x64
WorkspacePath          := "C:\Work\Dev\orx-projects\orximagemap\build\windows\codelite"
ProjectPath            := "C:\Work\Dev\orx-projects\orximagemap\build\windows\codelite"
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=19/08/2020
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/orxImageMapd.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ $(PreprocessorSwitch)__orxSTATIC__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="orxImageMap.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)$(ORX)/../extern/stb_image $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd $(LibrarySwitch)winmm 
ArLibs                 :=  "orxd" "winmm" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/static $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/ar.exe rcu
CXX      := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
CC       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -msse2 -m64 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -msse2 -m64 $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
AR:=x86_64-w64-mingw32-gcc-ar
Objects0=$(IntermediateDirectory)/src_orxImageMap.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_orxImageMap.c$(ObjectSuffix): ../../../src/orxImageMap.c $(IntermediateDirectory)/src_orxImageMap.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Work/Dev/orx-projects/orximagemap/src/orxImageMap.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_orxImageMap.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_orxImageMap.c$(DependSuffix): ../../../src/orxImageMap.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_orxImageMap.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_orxImageMap.c$(DependSuffix) -MM "../../../src/orxImageMap.c"

$(IntermediateDirectory)/src_orxImageMap.c$(PreprocessSuffix): ../../../src/orxImageMap.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_orxImageMap.c$(PreprocessSuffix) "../../../src/orxImageMap.c"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


