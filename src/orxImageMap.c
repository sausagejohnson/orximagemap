/**
 * @file orxImageMap.cpp
 * @date 17-August-2020
 */
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "orx.h"
#include "stb_image_write.h"

#if defined(__orxX86_64__) || defined(__orxPPC64__) || defined(__orxARM64__)
  #define CAST_HELPER (orxU64)
#else /* __orxX86_64__ || __orxPPC64__ || __orxARM64__ */
  #define CAST_HELPER (orxU32)
#endif /* __orxX86_64__ || __orxPPC64__ || __orxARM64__ */

const orxSTRING VERSION = "0.2";

struct __MetaData_t {
	orxSTRING		mapSectionName;
	orxSTRING		textureSectionName;
	orxVECTOR		vMapSize; //size of the actual image map for export.
							  //A 900 tile map in data could be 30x30, 10x90, 90x10, etc
							  //Must be supplied via commandline or set in the map data as
							  //the "Size" property.
							  //If neither of these, it will infer it from the map data row length.
	orxVECTOR		vTileSize; //size of each individual tile
	orxVECTOR		vTilesetTextureSize; //size of the bitmap texture containing tileset (used portion)
	orxSTRING		tileSetTextureFile;
	orxHASHTABLE	*pstIndexMap;
	orxSTRING		imageOutputFilePath;
	orxSTRING		configOutputFilePath;
	orxU32			totalTilesInMapData;
} MetaData;

orxBOOL iParamPassed = orxFALSE;
orxBOOL mParamPassed = orxFALSE;
orxBOOL zParamPassed = orxFALSE;

static orxSTATUS orxFASTCALL InputConfigPathHandler(orxU32 _u32ParamCount, const orxSTRING _azParams[]){
	orxSTATUS success = orxSTATUS_SUCCESS;
	iParamPassed = orxTRUE;
	
	//At least two params expected for width: the -w and the value
	if (_u32ParamCount < 2) {
		orxLOG("Not enough parameters found for loading the input config file.");
		return orxSTATUS_FAILURE;
	}
 
	orxConfig_Load(_azParams[1]);

	return orxSTATUS_SUCCESS;
}

static orxSTATUS orxFASTCALL MapSectionPathHandler(orxU32 _u32ParamCount, const orxSTRING _azParams[]){
	orxSTATUS success = orxSTATUS_SUCCESS;
	mParamPassed = orxTRUE;
	
	//At least two params expected for map config file
	if (_u32ParamCount < 2) {
		orxLOG("Not enough parameters found for specifying the map section name.");
		return orxSTATUS_FAILURE;
	}
 
	orxLOG("Map section to use is: %s", _azParams[1]);
	MetaData.mapSectionName = orxString_Duplicate(_azParams[1]);

	return orxSTATUS_SUCCESS;
}


static orxSTATUS orxFASTCALL ImageOutputPathHandler(orxU32 _u32ParamCount, const orxSTRING _azParams[]){
	orxSTATUS success = orxSTATUS_SUCCESS;
	
	//At least two params expected for png output: the -p and the value
	if (_u32ParamCount < 2) {
		orxLOG("Not enough parameters found for specifying the image output.");
		return orxSTATUS_FAILURE;
	}
 
	orxLOG("Output filepath for the PNG file is %s", _azParams[1]);
	MetaData.imageOutputFilePath = orxString_Duplicate(_azParams[1]);

	return orxSTATUS_SUCCESS;
}

static orxSTATUS orxFASTCALL ConfigOutputPathHandler(orxU32 _u32ParamCount, const orxSTRING _azParams[]){
	orxSTATUS success = orxSTATUS_SUCCESS;
	
	//At least two params expected for width: the -w and the value
	if (_u32ParamCount < 2) {
		orxLOG("Not enough parameters found for specifying the output config file.");
		return orxSTATUS_FAILURE;
	}
 
	orxLOG("Filepath output for the config file is %s", _azParams[1]);
	MetaData.configOutputFilePath = orxString_Duplicate(_azParams[1]);

	return orxSTATUS_SUCCESS;
}

static orxSTATUS orxFASTCALL MapSizeHandler(orxU32 _u32ParamCount, const orxSTRING _azParams[]){
	orxSTATUS success = orxSTATUS_SUCCESS;
	zParamPassed = orxTRUE;
	
	//At least two params expected for map config file
	if (_u32ParamCount < 2) {
		orxLOG("Not enough parameters found for specifying the map size.");
		return orxSTATUS_FAILURE;
	}
 
	orxLOG("Map size is set to: %s", _azParams[1]);
	
	orxSTRING sizeString = orxString_Duplicate(_azParams[1]);
	int width = 0;
	int height = 0;
	orxString_Scan(sizeString, "%dx%d", &width, &height);

	if (width == 0 | height == 0) {
		orxLOG("The Size argument is not formatted correctly. It must be two values separated by x, eg: -z 300x200");
		return orxSTATUS_FAILURE;
	}
	
	orxVECTOR sizeOfMap = {};
	sizeOfMap.fX = width;
	sizeOfMap.fY = height;

	MetaData.vMapSize = sizeOfMap;

	return orxSTATUS_SUCCESS;
}

orxBOOL IsSetupValid(){
	if (MetaData.mapSectionName == orxSTRING_EMPTY){
		orxLOG("No map section was found. Please ensure you specify a map section name using the -m parameter, and an input config is specified with the -i parameter.");
		return orxFALSE;
	} 
	
/*	if ( (MetaData.vMapSize.fX * MetaData.vMapSize.fY) != MetaData.totalTilesInMapData){
		orxLOG("The size of the expected map data and actual data does not match. Expected size: %f Actual size: %f", MetaData.totalTilesInMapData, (MetaData.vMapSize.fX * MetaData.vMapSize.fY));
		return orxFALSE;
	}*/
	
	if (MetaData.totalTilesInMapData = 0){
		orxLOG("No map data could be found under the section: [%s].", MetaData.mapSectionName);
		return orxFALSE;
	}
	
	if (MetaData.vMapSize.fX == 0 || MetaData.vMapSize.fY == 0)
	{
		orxLOG("Could not locate any rows of tiles in the map, or the rows are empty. Please check that your map is made up of rows of lists.");
		return orxFALSE;
	}
		
	return orxTRUE;
}

/*
 * This is the size of the texture that contains on all the tiles.
 * It is calculated without knowing about the texture image. It calculates
 * the used width and height by cycling through all tile sections using
 * the texture and getting the largest used dimensions 
 */
void CollectTileSheetTextureSize(){

	orxVECTOR       vTextureSetTL = {};
	orxVECTOR       vTextureSetBR = {};

	const orxSTRING zSetName;
	orxU32          i, u32Counter;

	orxConfig_PushSection(MetaData.textureSectionName);

	// Gets current section's name (for faster comparisons with orxConfig_GetParent() results)
	zSetName = orxConfig_GetCurrentSection();
	
	// For all config sections
	for (i = 0, u32Counter = orxConfig_GetSectionCount();
		i < u32Counter;
		i++){
		const orxSTRING zSectionName;

		// Gets its name
		zSectionName = orxConfig_GetSection(i);

		// Is a tile that belong to this set?
		if (orxConfig_GetParent(zSectionName) == zSetName)
		{
			orxVECTOR vTileOrigin = {};
			orxU32    u32TileIndex;

			// Selects it
			orxConfig_SelectSection(zSectionName);

			// Gets tile's origin (pixels)
			orxConfig_GetVector("TextureOrigin", &vTileOrigin);

			if (vTileOrigin.fX > vTextureSetBR.fX){
				vTextureSetBR.fX = vTileOrigin.fX;
			}
			
			if (vTileOrigin.fY > vTextureSetBR.fY){
				vTextureSetBR.fY = vTileOrigin.fY;
			}

		}
	}
	
	vTextureSetBR.fX += MetaData.vTileSize.fX;
	vTextureSetBR.fY += MetaData.vTileSize.fY;
	
	orxVector_Sub(&MetaData.vTilesetTextureSize, &vTextureSetBR, &vTextureSetTL);
	//orxVector_Div(&MetaData.vMapSize, &MetaData.vTilesetTextureSize, &MetaData.vTileSize) ;

	// Pops config section
	orxConfig_PopSection();
}

/*
 * Start with the Map Section, find a tile section,
 * then use the parent of the tile to get the Texture Object
 * that contains the TextureSize used by each Tile.
 * This information is required for calculating indexes.
 * 
 * Returns if successful
 */
orxSTATUS CollectDataStartingAtMapSection(){
	orxSTATUS success = orxSTATUS_SUCCESS; 
	MetaData.totalTilesInMapData = 0;
	
	//search section for first string list
	if (orxConfig_PushSection(MetaData.mapSectionName)){
		
		int i, rowCount = 0;
		orxU32 keyCount = orxConfig_GetKeyCount();
		
		// count the list rows in the map section
		// store the first string list for later searching
		const orxSTRING firstRowKey = orxSTRING_EMPTY;
		orxU16 firstRowLength = 0;
		
		//loop to count lists in the map to filter out non-list data from the row count
		for (i=0; i < keyCount; i++){
			const orxSTRING rowKey = orxConfig_GetKey(i);
			if (orxConfig_IsList(rowKey)){
				rowCount++;
				MetaData.totalTilesInMapData += orxConfig_GetListCount(rowKey);
				if (firstRowKey == orxSTRING_EMPTY){
					firstRowKey = orxString_Duplicate(rowKey);
					firstRowLength = orxConfig_GetListCount(rowKey);
				}
			} else { //is it the final map Size property? Only if not already overridden with cmd param
				if (MetaData.vMapSize.fX == 0 && MetaData.vMapSize.fY == 0){
					if (orxString_Compare(rowKey, "Size") == 0){
						orxConfig_GetVector("Size", &MetaData.vMapSize);
					}
				}
			}
		}
		
		//If the user has not provided a Size property in the map data section of their config
		//and they have not passed one in via the commandline, the dimensions of the map will
		//be calculated using the number of columns in the first row, and the total rows
		if (MetaData.vMapSize.fX == 0 && MetaData.vMapSize.fY == 0){
			MetaData.vMapSize.fX = firstRowLength; //make it iterate all rows and fail on length mismatches
			MetaData.vMapSize.fY = rowCount;			
		}

		
		// Get first string value from the firstRowKey
		const orxSTRING mainObjectTextureSection = orxSTRING_EMPTY;
		if (rowCount > 0){
			const orxSTRING mapTileSectionString = orxConfig_GetListString(firstRowKey, 0);
			if (orxConfig_PushSection(mapTileSectionString)){
				
				//Use the tile section and try to get both the TextureSize and texture section
				//name via the parent structure (doesn't need any config walking)
				orxVECTOR defaultTileTextureSize = {};
				orxConfig_GetVector("TextureSize", &defaultTileTextureSize);

				const orxSTRING tileSetTextureSectionName = orxConfig_GetValueSource("TextureSize");
				
				//If there is nothing, try for a Graphic section in the tile section
				//and go up the tree from there.

				if (tileSetTextureSectionName == orxSTRING_EMPTY){
					//get the parent of this which gives the original object
					const orxSTRING graphicSection = orxConfig_GetString("Graphic");
					
					
					if (orxConfig_PushSection(graphicSection)){
						orxVECTOR textureSize = {};
						orxConfig_GetVector("TextureSize", &defaultTileTextureSize);

						tileSetTextureSectionName = orxConfig_GetValueSource("TextureSize");

						orxConfig_PopSection();

					}
				}
				
				if (tileSetTextureSectionName != orxSTRING_EMPTY){
					MetaData.textureSectionName = orxString_Duplicate(tileSetTextureSectionName);
					
					if (orxConfig_PushSection(tileSetTextureSectionName)){
						MetaData.tileSetTextureFile = orxString_Duplicate(orxConfig_GetString("Texture"));
						orxConfig_PopSection();
					}
					
					MetaData.vTileSize.fX = defaultTileTextureSize.fX;
					MetaData.vTileSize.fY = defaultTileTextureSize.fY;
				} else {
					success = orxSTATUS_FAILURE;
					orxLOG("Problem:");
					orxLOG("Section Tile name [%s] could not be found your config.", mapTileSectionString);
					orxLOG("Please check your configuration files.");
				}

			}
			orxConfig_PopSection();
			
		}
		
		orxConfig_PopSection();

		if (success){
			CollectTileSheetTextureSize();	
		}
		
	}
	
	//MetaData.imageOutputFilePath = orxNULL;
	//MetaData.configOutputFilePath = orxNULL;
	return success;
}

orxBOOL orxFASTCALL MySaveShaderConfigCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	// -> filters out everything but the "MapShader" section and the Texture Section
	return (
		orxString_Compare(_zSectionName, "MapShader") == 0 || orxString_Compare(_zSectionName, MetaData.mapSectionName) == 0
		) ? orxTRUE : orxFALSE;
}

void ConvertHashDataToBitmap(){

	orxVECTOR   vSize, vMapSize = {}, vScreenSize = {};
	orxU8      *pu8Data, *pu8Value;
	orxU32      i, j, u32BitmapWidth, u32BitmapHeight, mapSectionKeyCount;

	// Pushes its config section
	orxConfig_PushSection(MetaData.mapSectionName);//_zMapName);

	mapSectionKeyCount = orxConfig_GetKeyCount();

	// Gets its size (tiles)
	vSize.fX = MetaData.vMapSize.fX;
	vSize.fY = MetaData.vMapSize.fY;

	// Adjusts map size
	vMapSize.fX = vSize.fX;
	vMapSize.fY = vSize.fY;
	vMapSize.fZ = orxMath_Ceil(vSize.fX / orx2F(2.0f)) * orx2F(2.0f);

	// Computes texture size (using 2 bytes per index as we have less than 65536 tiles in the set)
	u32BitmapWidth  = (orxF2U(vSize.fX) + 1) / 2;
	u32BitmapHeight = orxF2U(vSize.fY);
	
	orxVECTOR vSetSize = { MetaData.vTilesetTextureSize.fX / MetaData.vTileSize.fX, 
						   MetaData.vTilesetTextureSize.fY / MetaData.vTileSize.fY, 0 };

	orxVECTOR vExampleResolution = {800, 600, 0};
	orxVECTOR vExampleCameraSize = {800, 600, 2};

	// Upgrades map to become its own graphic
	orxConfig_SetString("Texture", MetaData.tileSetTextureFile);
	orxConfig_SetString("Pivot", "center");

	// Setups the shader on the map itself, with all needed parameters
	orxConfig_SetString("Code", "@MapShader");
	orxConfig_SetString("ParamList", "@MapShader");
	orxConfig_SetVector("CameraSize", &vExampleCameraSize);
	orxConfig_SetVector("MapSize", &vMapSize);
	orxConfig_SetVector("TileSize", &MetaData.vTileSize);
	orxConfig_SetVector("SetSize", &vSetSize);
	orxConfig_SetString("Map", MetaData.imageOutputFilePath);
	orxConfig_SetVector("Resolution", &vExampleResolution);
	orxConfig_SetVector("CameraPos", &orxVECTOR_0);
	orxConfig_SetVector("Highlight", &orxVECTOR_0);
	orxConfig_SetFloat("HighlightAlpha", 0.1);
	orxConfig_SetString("ftime", "time");
	orxConfig_SetBool("UseCustomParam", orxTRUE);

	//Save the shadermap code to disk
	if (MetaData.configOutputFilePath == orxNULL){
		orxConfig_Save("shader.ini", orxFALSE, MySaveShaderConfigCallback);
	} else {
		orxConfig_Save(MetaData.configOutputFilePath, orxFALSE, MySaveShaderConfigCallback);
	}
	

	// Allocates bitmap data
	pu8Data = (orxU8 *)orxMemory_Allocate(u32BitmapWidth * u32BitmapHeight * sizeof(orxRGBA), orxMEMORY_TYPE_TEMP);
	orxASSERT(pu8Data);


	// For all rows
	for (j = 0, pu8Value = pu8Data; j < mapSectionKeyCount; j++)
	{

		// Gets row's name
		const orxSTRING keyName = orxConfig_GetKey(j);
		if (orxConfig_IsList(keyName)){
			orxU32 tilesInRowCount = orxConfig_GetListCount(keyName);
		
			// For all string list columns
			for (i = 0; i < orxF2U(tilesInRowCount); i++)
			{
				const orxSTRING zTile;
				orxU32          u32Index;

				// Pushes tile's section
				//orxLOG("Pushing section from row %s index %d", keyName, i);
				orxConfig_PushSection(orxConfig_GetListString(keyName, i));

				// Gets its name
				zTile = orxConfig_GetCurrentSection();

				// Pops config section
				orxConfig_PopSection();

				// Gets matching tile index
				//u32Index = (orxU32)CAST_HELPER orxHashTable_Get(MetaData.pstIndexMap, (orxU64)zTile);
				u32Index = (orxU32)CAST_HELPER orxHashTable_Get(MetaData.pstIndexMap, (orxU64)orxString_ToCRC(zTile)) - 1;
				
	orxU64 test = (orxU64)orxString_GetID(zTile);
	if (test == 2221536315){
		int here = 1;
	}
	
	if (orxString_Compare(zTile, "Tiles2") == 0){
		int here = 1;
	}

				// Stores it over two bytes
				*pu8Value++ = (u32Index & 0xFF00) >> 8;
				*pu8Value++ = u32Index & 0xFF;
			}
			
			// Zeroes padding bytes
			if (orxF2U(vSize.fX) & 1)
			{
				*pu8Value++ = 0;
				*pu8Value++ = 0;
			}
			
		}


	}

	int saveSuccess = 0;

	orxSTRING outputFile;
	if (MetaData.imageOutputFilePath !=orxNULL) {
		outputFile = orxString_Duplicate(MetaData.imageOutputFilePath);
	} else {
		outputFile = orxString_Duplicate("output-map.png");
	}

	saveSuccess = stbi_write_png(outputFile, u32BitmapWidth, u32BitmapHeight, 4, pu8Data, 0);
	if (saveSuccess > 0){
		orxLOG("The map image was successfully saved to: %s.", outputFile);
	} else {
		orxLOG("Saving the map image at: %s failed. Check if the path exists and you have rights to write to it.", outputFile);
	}


	// Deletes bitmap data
	orxMemory_Free(pu8Data);
	pu8Data = orxNULL;

	// Pops config section
	orxConfig_PopSection();

	// Done!
}

orxBOOL IsTileNameInMapData(const orxSTRING tileSectionName){
	if (orxConfig_PushSection(MetaData.mapSectionName)){
		
		int i, j, rowCount = 0;
		orxU32 keyCount = orxConfig_GetKeyCount();
		
		//loop to count lists in the map to filter out non-list data from the row count
		for (i=0; i < keyCount; i++){
			const orxSTRING rowKey = orxConfig_GetKey(i);
			if (orxConfig_IsList(rowKey)){
				rowCount++;
				orxU32 listIndex = orxConfig_GetListCount(rowKey);
				
				for (j=0; j<listIndex; j++){
					const orxSTRING tile = orxConfig_GetListString(rowKey, j);
					if (orxString_Compare(tile, tileSectionName) == 0 ){
						orxConfig_PopSection();
						return orxTRUE;
					}
				}
				
			}
		}
		
		orxConfig_PopSection();
		//orxLOG("Warning: %s was not found in the map data.", tileSectionName);
		return orxFALSE;
		
	}

}

//Returns the first object section that uses that Graphic Section
const orxSTRING getObjectSectionNameUsedByGraphicSectionName(const orxSTRING graphicSectionName){
	// For *all* config sections
	int i, u32Counter;
	for (i = 0, u32Counter = orxConfig_GetSectionCount();
		i < u32Counter;
		i++){
		const orxSTRING zSectionName;

		// Gets its name
		zSectionName = orxConfig_GetSection(i);
		
		if (orxConfig_PushSection(zSectionName)){
			if (orxConfig_HasValue("Graphic") ) //eg. Tile1Graphic has parent MyMainGraphic
			{
				const orxSTRING graphicThisObjectIsUsing = orxConfig_GetString("Graphic");
				if (orxString_Compare(graphicSectionName, graphicThisObjectIsUsing) == 0){
					orxConfig_PopSection();
					return zSectionName;
				}
				
			}
		}
		orxConfig_PopSection();
		
	}
	
	return orxSTRING_EMPTY;
}

void BuildHashFromCollectedData(){

	// Pushes its config section
	orxConfig_PushSection(MetaData.textureSectionName); //eg. MyMainGraphic
	orxVECTOR       vBitmapSetSize = {}; //size of hashtable index values, not final map bitmap export
	const orxSTRING zSetName;
	orxU32          i, u32Counter;


	orxVector_Div(&vBitmapSetSize, &MetaData.vTilesetTextureSize, &MetaData.vTileSize);

	//vBitmapSetSize.fX = MetaData.vMapSize.fX * MetaData.vTileSize.fX;
	//vBitmapSetSize.fY = MetaData.vMapSize.fY * MetaData.vTileSize.fY;

	MetaData.vTileSize.fZ = orxFLOAT_1;

	// Creates index map
	MetaData.pstIndexMap = orxHashTable_Create(orxF2U(vBitmapSetSize.fX * vBitmapSetSize.fY), orxHASHTABLE_KU32_FLAG_NONE, orxMEMORY_TYPE_MAIN);
	orxASSERT(MetaData.pstIndexMap);

	// Gets section's name (for faster comparisons with orxConfig_GetParent() results)
	zSetName = orxConfig_GetCurrentSection(); //eg. MyMainGraphic

	// For *all* config sections
	for (i = 0, u32Counter = orxConfig_GetSectionCount();
		i < u32Counter;
		i++){
		const orxSTRING zSectionName;

		// Gets its name
		zSectionName = orxConfig_GetSection(i);

		// Does this child Graphic Section belong to the MyMainGraphic (which is the TileSet graphic)
		if (orxConfig_GetParent(zSectionName) == zSetName) //eg. Tile1Graphic has parent MyMainGraphic
		{
			//check if section name is used in the tilemap data at all
			//If not, warn and count. In this case, try for the object that uses the graphic
			
			const orxSTRING objectSectionThatUsesThisGraphic;
			
			
			orxVECTOR vTileOrigin = {};
			orxU32    u32TileIndex;

			// Selects it
			orxConfig_SelectSection(zSectionName);

			// Gets tile's origin (tile origin)
			orxConfig_GetVector("TextureOrigin", &vTileOrigin);

			//If this section is not found in the map then you are working on a 
			//graphic, and the map is likely refering objects that USE
			//those graphic section instead. In this case, switch to the 
			//object that uses the graphic. Could this be problematic
			//if more than one object uses the same graphic? Unlikely if came
			//from the TMX to Orx converter.
			orxBOOL isInMap = IsTileNameInMapData(zSectionName);
			if (isInMap == orxFALSE){
				zSectionName = getObjectSectionNameUsedByGraphicSectionName(zSectionName);
			}

			// Converts tile's origin (into pixel units)
			orxVector_Round(&vTileOrigin, orxVector_Div(&vTileOrigin, &vTileOrigin, &MetaData.vTileSize));

			// Computes its index
			u32TileIndex = orxF2U(vTileOrigin.fX + (vBitmapSetSize.fX * vTileOrigin.fY)) + 1;

		
			orxU64 u64SectionNameGUID = (orxU64)orxString_ToCRC(zSectionName);
			void *u64CastTileIndex = (void *) CAST_HELPER u32TileIndex;

			// Stores it
			//orxHashTable_Add(MetaData.pstIndexMap, (orxU64)orxString_GetID(zSectionName), (void *) CAST_HELPER u32TileIndex);
			orxHashTable_Add(MetaData.pstIndexMap, u64SectionNameGUID, (void *) CAST_HELPER u32TileIndex);
		}
	}

	// Pops config section
	orxConfig_PopSection();

	// Done!
}


orxSTATUS orxFASTCALL Run()
{
  orxSTATUS eResult = orxSTATUS_SUCCESS;

	if (MetaData.mapSectionName != orxNULL){
		if (!orxConfig_HasSection(MetaData.mapSectionName)){
			orxLOG("Map section [%s] not was found in your config.", MetaData.mapSectionName);
			return orxSTATUS_FAILURE;
		}
		
		orxLOG("Map section [%s] is found.", MetaData.mapSectionName);
		
		CollectDataStartingAtMapSection();
		if (IsSetupValid()){
			BuildHashFromCollectedData();
			ConvertHashDataToBitmap();
		} else {
			orxLOG("Please correct the issues above and try again.");
		}
		
	} else {
		orxLOG("A map section name was not specified. Please use the -m (--mapsection) parameter.");
		return orxSTATUS_FAILURE;
	}

	// Done!
	return eResult;
}


void orxFASTCALL Exit()
{
    // Let Orx clean up the mess automatically. :)
}


/** Init function, it is called when all orx's modules have been initialized
 */
orxSTATUS orxFASTCALL Init()
{
	orxLOG("OrxImageMap version %s", VERSION);

	orxPARAM stInputParam = {orxPARAM_KU32_FLAG_STOP_ON_ERROR, "i", "input", "Config file containing map data.", "Path to an Orx config file that contains map data.", &InputConfigPathHandler};
	orxPARAM stMapSectionParam = {orxPARAM_KU32_FLAG_STOP_ON_ERROR, "m", "mapsection", "Name of map section.", "Name of the section that contains the map to convert.", &MapSectionPathHandler};
	orxPARAM stPngOutputParam = {orxPARAM_KU32_FLAG_STOP_ON_ERROR, "o", "png", "Filepath to output PNG", "Filepath to the map PNG to save to.", &ImageOutputPathHandler};
	orxPARAM stConfigOutputParam = {orxPARAM_KU32_FLAG_STOP_ON_ERROR, "s", "sections", "Filepath to output config sections", "Filepath to save a config file containing shader and map sections.", &ConfigOutputPathHandler};
	orxPARAM stMapSizeParam = {orxPARAM_KU32_FLAG_STOP_ON_ERROR, "z", "size", "Dimension size of final Map, eg: 50x70", "The size dimension of your mapdata, for example: 50x70.", &MapSizeHandler};
	
	orxSTATUS status_i = orxParam_Register(&stInputParam);
	if (status_i == orxSTATUS_SUCCESS){
		if (!iParamPassed){
			orxLOG("Input -i was not found. This argument is required.");
		}
	}
	orxSTATUS status_m = orxParam_Register(&stMapSectionParam);
	if (status_m == orxSTATUS_SUCCESS){
		if (!mParamPassed){
			orxLOG("Map -m was not found. This argument is required.");
		}
	}
	orxParam_Register(&stPngOutputParam);
	orxParam_Register(&stConfigOutputParam);
	orxSTATUS status_z = orxParam_Register(&stMapSizeParam);
	if (status_z == orxSTATUS_FAILURE){
		if (!zParamPassed){
			orxLOG("Size -z was not supplied. This value will be taken from the Size parameter in your map data config section.");
		}
	}

    // Done!
    return orxSTATUS_SUCCESS;
}

static void orxFASTCALL Setup()
{
	  // Adds module dependencies
	  orxModule_AddDependency(orxMODULE_ID_MAIN, orxMODULE_ID_PARAM);
	  orxModule_AddDependency(orxMODULE_ID_MAIN, orxMODULE_ID_FILE);
	  
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS orxFASTCALL Bootstrap()
{
    // Add a config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);
	orxConfig_Load("exports.ini");
 
    // Return orxSTATUS_FAILURE to prevent this tool from loading the default config file based on executable name
    return orxSTATUS_FAILURE;
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Set the bootstrap function to provide at least one resource storage before loading any config files
    orxConfig_SetBootstrap(Bootstrap);

	orxDEBUG_INIT();

	orxModule_Register(orxMODULE_ID_MAIN, "MAIN", Setup, Init, Exit);

	if (orxParam_SetArgs(argc, argv) != orxSTATUS_FAILURE){
		if(orxModule_Init(orxMODULE_ID_MAIN) != orxSTATUS_FAILURE){

			Run();

			orxModule_Exit(orxMODULE_ID_MAIN);
		} else {
			orxLOG("Initialising the modules inside Orx failed.");
		}
	}

	orxDEBUG_EXIT();

	// Done!
	return EXIT_SUCCESS;
}
