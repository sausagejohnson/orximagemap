# orxImageMap

This command line tool is used to accept level map data from an Orx configuration file and to create a PNG file based on the tile data. This PNG can then be used to render tiled maps within a shader for faster scrolling.

Options:

-i        --input                    Config containing map data.\
-m        --mapsection               Name of map section.\
-o        --png                      Filepath to output PNG\
-s        --sections                 Filepath to output config sections\
-h        --help                     Prints this help. A parameter can be specified to print its complete description (-h <param>).

Example usage:

orxImageMap	-i c:\work\some-project-with-a-map\config\game-map.ini \
-m MyGameMap \
-o c:\work\some-project-with-a-map\images\map.png \
-s 
c:\work\some-project-with-a-map\config\override-sections.ini

A handy workflow for a project is to set up a batch or shell script to run this command from your project folder to re-generate the latest map image to match changes in your map config.

These instructions will be fleshed out properly soon.